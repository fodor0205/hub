#!/bin/bash
set -e

# Waiting for file socket to be free
function wait_for_sock() {
  try=0
  while test $try -lt 80 ; do
    if [[ ! -S $1 && ! -f "$1" ]]; then
      try=''
      break
    fi
    echo -n .
    try=$(expr $try + 1)
    sleep 0.2
  done

  if [[ -S $1 || -f $1 ]]; then
    echo ""
    echo Removing PHP socket file...
    rm -f $1
  fi
}

if [ ! "$(pidof php-fpm)" ]; then
  PHP_FPM_SOCK=${PHP_FPM_SOCK:-/var/run/php/php-fpm.sock}
  wait_for_sock $PHP_FPM_SOCK
  exec php-fpm
fi || true
